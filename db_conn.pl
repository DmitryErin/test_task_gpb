#!/usr/bin/perl -w

use strict;
use POSIX;
use DBI;

sub conn {
    my $host = "1.2.3.4";		# имя (ip) MySQL-сервера
    my $port = "3306";			# порт, на который открываем соединение
    my $user = "user_name";		# имя пользователя бд
    my $pass = "user_pass";		# пароль бд
    my $db = "new";			# имя базы данных
    
    my $dbh = DBI->connect("DBI:mysql:database=$db;host=$host;port=$port", $user,$pass, { RaiseError => 1 }) or die DBI->errstr ; # подключаемся к бд

    return $dbh;
}
