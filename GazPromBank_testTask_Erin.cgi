#!/usr/bin/perl

local ($buffer, @pairs, $pair, $name, $value, %FORM);
$ENV{'REQUEST_METHOD'} =~ tr/a-z/A-Z/;

if ($ENV{'REQUEST_METHOD'} eq "POST") {
   read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
} else {
   $buffer = $ENV{'QUERY_STRING'};
}

@pairs = split(/&/, $buffer);

foreach $pair (@pairs) {
   ($name, $value) = split(/=/, $pair);
   $value =~ tr/+/ /;
   $FORM{$name} = $value;
}

$email = $FORM{'email'};

print "Content-type:text/html\r\n\r\n";
print "<html>";
print "<head>";
print "<title>Тестовое задание</title>";
print "</head>";
print "<body>";

print '<FORM action = "" method="POST">
   введите e-mail адрес: <input type="text" name="email">
   <input type="submit" value="Submit">
</FORM>';

if ($ENV{'REQUEST_METHOD'} eq "POST" && $email != '') {

    require "./db_conn.pl";

    my $dbh = &conn;
    
    $qntLog = "SELECT count(*) as logQnt from log where address = ". $dbh->quote($email);
    $qntMes = "SELECT count(*) as mesQnt from message where int_id in (select a.int_id from log a where a.address = ".$dbh->quote($email).")";

    my $sth = $dbh->prepare($qntLog);
    $sth->execute;
    while ($ref = $sth->fetchrow_hashref) {
	$qnt_log = $ref->{'logQnt'};
    }

    $sth = $dbh->prepare($qntMes);
    $sth->execute;
    while ($ref = $sth->fetchrow_hashref) {
	$qnt_mes = $ref->{'mesQnt'};
    }
    
    if (($qnt_log + $qnt_mes) > 100) {
	$warnString = 'Внимание! Результат содержит '.($qnt_log + $qnt_mes).' строк. На эксран выведено первые 100.';
    } else {$warnString = '';}
    
    $request = "(SELECT `created`, `str` FROM `log` WHERE `address` = ". $dbh->quote($email)." order by `created`, `int_id`)
	union all
	(SELECT `created`, `str` FROM `message` where `int_id` in (SELECT `int_id` FROM `log` WHERE `address` = ".$dbh->quote($email)." order by `created`, `int_id`))
	order by `created`";
	
    $sth = $dbh->prepare($request);
    $sth->execute;
    
    if (($qnt_log + $qnt_mes) > 0) {

	print "<h2>Список сообщений</h2>";
	print "<h3><font color='red'>" .$warnString. "</font></h3>";
	print "<table>";

	while ($row = $sth->fetchrow_hashref) {												# итерация по полученному из бд массиву записей
	    print "<tr><td>".$row->{'created'}."</td><td>".$row->{'str'}."</td></tr>";							# выводим таблицу с записями на экран
	}

	print "</table>";
    }
}

print "</body></html>";

1;
